Projet web AOM disponible à l'adresse suivante : https://aom.zoejeulin.fr/
2020 © Baptiste Desgouttes, Evan-James Duvinage, Zoé Jeulin

Age Of Mythology est un célèbre jeu de stratégie en temps réel qui fait vibrer des générations depuis 2002.
Dans la lignée des Age of Empires, il a la particularité de mettre en scène 5 civilisations : grecque, égyptienne, scandinave, atlantéenne et chinoise.  
Le jeu permet d’incarner des divinités issues de ces mythologies pour diriger leur civilisation et vaincre les ennemis (AI, local ou en ligne). 

Objectifs du site : 
Le projet de ce site était de créer une encyclopédie en ligne permettant d'accéder à tout le savoir nécessaire autour de ce jeu.
Ce projet a été réalisé dans le cadre du cours de PHP de l'IMAC, afin de mettre en pratique :
- une architecture REST,
- l'utilisation d'une base de données,
- le remplissage dynamique de pages grâce à l'utilisation de Javascript.

Le site se découpe en 2 parties : "Civilisations" et "Maps"

I - Civilisations
    A) Choix de la civilisation
        Une première page permet de choisir la civilisation dont on souhaite avoir plus de détails
    B) Détails sur une civilisation
        Une fois sur la page d'une civilisation, celle-ci est découpée en 3 grandes parties : "Dieux", "Unités" et "Bâtiments".
        
        - La partie "Dieux" présente tout d'abord les dieux primaires de cette civilisation.
        En cliquant sur l'un d'entre-eux, on peut afficher les dieux qu'il est possible d'avoir au fil des âges dans la partie en la commençant avec ce dieux.
        Pour chacun de ces dieux, il est possible d'avoir plus d'informations en cliquant dessus, ce qui ouvre une boîte modale.
        A l'intérieur de celle-ci se trouvent différentes informations dont une description du dieu ainsi que de ses pouvoirs utilisables en jeu.
        
        - La partie "Unités" présente toutes les unités disponibles avec cette civilisation, classées selon l'âge à partir duquel elles sont disponibles dans le jeu.
        Pour chacune de ces unités, il est possible d'avoir plus d'informations en cliquant dessus, ce qui ouvre une boîte modale.
        A l'intérieur de celle-ci se trouvent différentes informations dont une description de l'unité ainsi que ses coûts dans les différentes ressources du jeu.
        
        - La partie "Bâtiments" présente tous les bâtiments disponibles avec cette civilisation, classés selon l'âge à partir duquel ils sont disponibles dans le jeu.
        Pour chacun de ces bâtiments, il est possible d'avoir plus d'informations en cliquant dessus, ce qui ouvre une boîte modale.
        A l'intérieur de celle-ci se trouvent différentes informations dont une description du bâtiment, ses coûts dans les différentes ressources du jeu ainsi qu'une image des améliorations possibles pour ce bâtiment.
        Au survol d'une de ces améliorations, de plus amples informations s'affichent (nom, description et coûts).

II - Maps
    La partie "Maps" contient une liste de toutes les maps qu'il est possible de choisir dans le jeu.
    Pour chacune d'entre-elles, il est possible d'avoir plus d'informations en cliquant dessus, ce qui ouvre une boîte modale.
    A l'intérieur de celle-ci se trouvent également 2 boutons : 
    - le premier permet d'ajouter cette map aux favorites (sur le site, fonctionnalité implémentée pour mettre en pratique le cas d'un "PUT"),
    - le deuxième permet de la supprimer (sur le site également, fonctionnalité implémentée simplement pour mettre en pratique le cas d'un "DELETE").
    
    En haut de cette page se trouvent 3 boutons :
    - le premier permet de trier les maps par ordre alphabétique,
    - le deuxième permet de trier les maps en affichant en priorité celles ajoutées aux favorites,
    - le dernier bouton permet de réinitialiser l'affichage des maps en faisant réapparaître les maps supprimées.