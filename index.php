<?php
// Initialisation des paramètres du site
require_once('./config/configuration.php');
require_once('./lib/foncBase.php');
require_once(PATH_TEXTES.LANG.'.php');

if($_SERVER['REQUEST_URI']=="/AOM/index.php/"){ //redirige vers l'URL sans / à la fin
	header("Location:/AOM/index.php");
}
if($_SERVER['REQUEST_URI']=="/AOM/index.php/maps/"){ //redirige vers l'URL sans / à la fin
	header("Location:/AOM/index.php/maps");
}
if($_SERVER['REQUEST_URI']=="/AOM/index.php/civilisations/"){ //redirige vers l'URL sans / à la fin
	header("Location:/AOM/index.php/civilisations");
}

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); //récupère l'URL après localhost. Ex. : "/AOM/index.php/maps"
$uri = explode( '/', $uri ); //crée un tableau de chaque élément de l'URL entre les / 

//Fait office de routeur
switch(sizeof($uri)){
	case 3:
		$page='accueil';
		break;
	case 4:
		if($uri[3]=='maps'){
			$page='maps';
		}
		if($uri[3]=='civilisations'){
			$page='civilisations';
		}
		break;
	case 5:
		$page='detailsCivilisation';
		break;
	default:
		$page='accueil';
		break;
}

//appel de la vue
require_once(PATH_VIEWS.$page.'.php');
?>
