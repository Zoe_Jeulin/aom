<?php
require_once('../config/configuration.php');
require_once(PATH_MODELS."CivilisationDAO.php");

$c = new CivilisationDAO(DEBUG); //création d'un "objet" Civilisation (avec ses fonctions etc)

$requestMethod=$_SERVER["REQUEST_METHOD"]; //récupère la méthode de la requête (GET, POST, PUT, DELETE)

switch($requestMethod){
	case 'GET':
		$response = $c->getAllCivilisation(); //récupère toutes les civilisations
		break;
	default:
		break;
}

echo $response;
?>