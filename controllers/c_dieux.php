<?php
require_once('../config/configuration.php');
require_once(PATH_MODELS."CivilisationDAO.php");
require_once(PATH_MODELS."DieuDAO.php");

$c = new CivilisationDAO(DEBUG);
$d = new DieuDAO(DEBUG);

$url = explode( '/', $_SERVER['HTTP_REFERER'] ); //crée un tableau de chaque élément de l'URL entre les / 

$nomCivilisationRequest=$url[6]; //récupère le nom de la civilisations à afficher

$requestMethod=$_SERVER["REQUEST_METHOD"]; //récupère la méthode de la requête (GET, POST, PUT, DELETE)

switch($requestMethod){
	case 'GET': //récupération des dieux primaires d'une civilisation
		$idCivilisationRequest = $c->getCivilisationId($nomCivilisationRequest); //récupération de l'id de la civilisation
		$response = $d->getDieuxPrimairesFromCivilisation($idCivilisationRequest); //récupération des dieux
		break;
	case 'POST': //récupération des dieux secondaires d'un dieu primaire
		$json=file_get_contents('php://input');
		$parametresRequete=json_decode($json,TRUE);

		$idDieuPrimaire = $parametresRequete['idDieuPrimaire'];
		$idDieuxSecondaires = explode('_',$parametresRequete['idDieuxSecondaires']);

		$idCivilisationRequest = $c->getCivilisationId($nomCivilisationRequest); //récupération de l'id de la civilisation
		$response = $d->getDieuxSecondairesByIdString($idCivilisationRequest,$idDieuPrimaire,$idDieuxSecondaires); //récupération des dieux
		break;
	default:
		break;
}

echo $response;
?>