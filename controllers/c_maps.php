<?php
require_once('../config/configuration.php');
require_once(PATH_MODELS."MapDAO.php");

$m = new MapDAO(DEBUG); //création d'un "objet" Map (avec ses fonctions etc)

$requestMethod=$_SERVER["REQUEST_METHOD"]; //récupère la méthode de la requête (GET, POST, PUT, DELETE)

switch($requestMethod){
	case 'GET':
		//
		break;
	case 'POST':
		$json=file_get_contents('php://input');
        $btnPressed=json_decode($json,TRUE);
		if($btnPressed==2){
			$response = $m->getAllMapOrderFav(); //récupère toutes les maps
		}else if($btnPressed==3){
			$response = $m->reinitMaps();
			$response = $m->getAllMapOrderFav();
		}else{
			$response = $m->getAllMapOrderAlpha();
		}
		break;
	case 'PUT':
		$json=file_get_contents('php://input');
        $idMapRequest=json_decode($json,TRUE);
		$response = $m->getMapById($idMapRequest);
		if($response['enFavori']==0){
			$response = $m->addMapToFavorite($idMapRequest);
		}else{
			$response = $m->removeMapFromFavorite($idMapRequest);
		}
		
		$response = $m->getAllMapOrderAlpha();
		break;
	case 'DELETE':
		$json=file_get_contents('php://input');
        $idMapRequest=json_decode($json,TRUE);
		$response = $m->deleteMap($idMapRequest);
		$response = $m->getAllMapOrderAlpha();
		break;
	default:
		//$response = notFound(); //à faire plus tard
		break;
}

echo $response;
?>
