<?php
require_once('../config/configuration.php');
require_once(PATH_MODELS."AmeliorationDAO.php");

$a = new AmeliorationDAO(DEBUG);

$requestMethod=$_SERVER["REQUEST_METHOD"]; //récupère la méthode de la requête (GET, POST, PUT, DELETE)

switch($requestMethod){
    case 'POST': //récupération des améliorations d'un bâtiment
        $json=file_get_contents('php://input');
        $idBatiment=json_decode($json,TRUE);
		$response = $a->getBatimentsAmeliorations($idBatiment); 
		break;
	default:
		break;
}

echo $response;
?>