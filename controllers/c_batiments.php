<?php
require_once('../config/configuration.php');
require_once(PATH_MODELS."CivilisationDAO.php");
require_once(PATH_MODELS."BatimentDAO.php");

$c = new CivilisationDAO(DEBUG);
$b = new BatimentDAO(DEBUG);

$url = explode( '/', $_SERVER['HTTP_REFERER'] ); //crée un tableau de chaque élément de l'URL entre les / 

$nomCivilisationRequest=$url[6]; //récupère le nom de la civilisations à afficher

$requestMethod=$_SERVER["REQUEST_METHOD"]; //récupère la méthode de la requête (GET, POST, PUT, DELETE)

switch($requestMethod){
	case 'GET': //récupération des bâtiments d'une civilisation
		$idCivilisationRequest = $c->getCivilisationId($nomCivilisationRequest); //récupération de l'id de la civilisation
		$response = $b->getBatimentsFromCivilisation($idCivilisationRequest); //récupération des batiments
		break;
	default:
		break;
}

echo $response;
?>