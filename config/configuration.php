<?php
 
const DEBUG = true; // production : false; dev : true

// Accès base de données
/*const BD_HOST = 'db5000376489.hosting-data.io';
const BD_DBNAME = 'dbs363858';
const BD_USER = 'dbu363084';
const BD_PWD = 'AoMBEZ!2020';*/

const BD_HOST = 'localhost';
const BD_DBNAME = 'aom';
const BD_USER = 'root';
const BD_PWD = '';

// Langue du site
const LANG ='FR-fr';

// Paramètres du site : nom de l'auteur ou des auteurs
const AUTEUR = 'Baptiste DESGOUTTES, Evan-James DUVINAGE, Zoé JEULIN'; 

//dossiers racines du site
define('PATH_CONTROLLERS','./controllers/c_');
define('PATH_ENTITIES','./entities/');
define('PATH_ASSETS','/AOM/assets/');
define('PATH_LIB','./lib/');
define('PATH_MODELS','../models/');
define('PATH_VIEWS','./views/v_');
define('PATH_TEXTES','./languages/');

//sous dossiers
define('PATH_CSS', PATH_ASSETS.'css/');
define('PATH_IMAGES', PATH_ASSETS.'images/');
define('PATH_SCRIPTS', PATH_ASSETS.'scripts/');

define('PATH_IMG_MAPS', PATH_IMAGES.'maps/');
define('PATH_IMG_DIEUX', PATH_IMAGES.'dieux/');
define('PATH_IMG_CIVILISATIONS', PATH_IMAGES.'civilisations/');

