<?php require_once(PATH_VIEWS.'headerDetailsCivilisation.php');?>

<!--  Corps de la page -->

<div id="contentDivDetailsCivilisation" class="row">
	<div class="col s12">
		<ul id="tabs-swipe-demo" class="tabs tabs-fixed-width">
			<li id="ongletDieux" class="tab col s3"><a class="active" href="#dieux">Dieux</a></li>
			<li id="ongletUnites" class="tab col s3"><a href="#unites">Unités</a></li>
			<li id="ongletBatiments" class="tab col s3"><a href="#batiments">Bâtiments</a></li>
		</ul>
	</div>

	<?php require_once(PATH_VIEWS.'dieux.php');?>
	<?php require_once(PATH_VIEWS.'unites.php');?>
	<?php require_once(PATH_VIEWS.'batiments.php');?>
	
</div>
	
<!--  Footer -->
<?php require_once(PATH_VIEWS.'footer.php'); ?>