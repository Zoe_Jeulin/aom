<?php require_once(PATH_VIEWS.'headerMaps.php');?>

<!--  Corps de la page -->
<div id="div_maps_globale" class="row">
	<div id="btn_maps" class="center-align">
		<a id="btn_ordre_alpha" class="waves-effect waves-light btn">Trier par ordre alphabétique</a>
		<a id="btn_ordre_fav" class="waves-effect waves-light btn">Trier par favoris</a>
		<a id="btn_reinit" class="waves-effect waves-light btn">Réinitialiser</a>
	</div>
	<div id="liste-maps">
	</div>

</div>

<!--  Footer -->
<?php require_once(PATH_VIEWS.'footer.php'); ?>
