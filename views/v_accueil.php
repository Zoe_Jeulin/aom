<?php require_once(PATH_VIEWS.'header.php');?>

<!--  Corps de la page -->
<div class="row" id="div_accueil_content">
	<div class="col s6 valign-wrapper center-align" id="col_civilisations" onclick="window.location.href='index.php/civilisations'">
		<a href="index.php/civilisations" class="liens_page_accueil">CIVILISATIONS</a>
	</div>
	<div class="col s6 valign-wrapper center-align" id="col_maps" onclick="window.location.href='index.php/maps'">
		<a href="index.php/maps" class="liens_page_accueil">MAPS</a>
	</div
</div>

<!--  Footer -->
<?php require_once(PATH_VIEWS.'footer.php'); ?>
