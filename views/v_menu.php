<!-- Menu du site -->
<!-- <nav></nav> -->

<div class="row valign-wrapper" id="div_accueil_header">
    <div class="col s1 center-align">
		<ul id="slide-out" class="sidenav left-align">
			<li><a id="titre_site_menu">AGE OF MYTHOLOGY</a></li>
			<li><a href="/AOM/index.php">ACCUEIL</a></li>
			<li class="no-padding">
				<ul class="collapsible collapsible-accordion">
					<li>
						<a class="collapsible-header" id="civilisations_menu">CIVILISATIONS<i class="material-icons">arrow_drop_down</i></a>
						<div class="collapsible-body">
							<ul>
								<li><a href="/AOM/index.php/civilisations" class="lien_civilisation_menu">Tout voir</a></li>
								<li><a href="/AOM/index.php/civilisations/grecs" class="lien_civilisation_menu">Grecs</a></li>
								<li><a href="/AOM/index.php/civilisations/egyptiens" class="lien_civilisation_menu">Egyptiens</a></li>
								<li><a href="/AOM/index.php/civilisations/scandinaves" class="lien_civilisation_menu">Scandinaves</a></li>
								<li><a href="/AOM/index.php/civilisations/atlanteens" class="lien_civilisation_menu">Atlantéens</a></li>
								<li><a href="/AOM/index.php/civilisations/chinois" class="lien_civilisation_menu">Chinois</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</li>			
			<li><a href="/AOM/index.php/maps">MAPS</a></li>
		</ul>
		<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
	<div class="col s10" id="div_titre_header">
		<h1 id="titre_header"><a href="/AOM/index.php">AGE OF MYTHOLOGY</a></h1>
	</div>
	<div class="col s1">
	</div>
</div>