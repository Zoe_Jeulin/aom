<?php
//Isoler ici dans des constantes les textes affichés sur le site
define('LOGO',''); // Affiché si image non trouvée
define('TITRE','AOM');
define('BOUTON_VALIDER','Valider');

//Messages d'erreurs
define('ERREUR_QUERY','Problème d\'accès à la base de données. Contactez l\'administrateur');
define('TEXTE_PAGE_404','Oups, la page demandée n\'existe pas !');
define('MESSAGE_ERREUR','Une erreur s\'est produite');

//Messages d'information


?>