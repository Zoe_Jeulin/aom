<?php

require_once (PATH_MODELS."DAO.php");

class MapDAO extends DAO {
	
	//Récupérer toutes les maps par ordre alphabétique
	public function getAllMapOrderAlpha(){
		$result = $this->queryAll('SELECT * FROM aom_map ORDER BY nomMap ASC');
		return json_encode($result);
	}
	
	//Récupérer toutes les maps par ordre favori
	public function getAllMapOrderFav(){
		$result = $this->queryAll('SELECT * FROM aom_map ORDER BY enFavori DESC');
		return json_encode($result);
	}

	//Récupérer une seule map grâce à son id
	public function getMapById($idMapRequest){
		$result = $this->queryRow('SELECT * FROM aom_map WHERE idMap=?;',array($idMapRequest));
		return $result;
	}
	
	//Ajouter une map aux favorites
	public function addMapToFavorite($idMapRequest){		
		$result = $this->_requete('UPDATE aom_map SET enFavori=1 WHERE idMap=?;',array($idMapRequest));
	}
	
	//Supprimer une map des favorites
	public function removeMapFromFavorite($idMapRequest){		
		$result = $this->_requete('UPDATE aom_map SET enFavori=0 WHERE idMap=?;',array($idMapRequest));
	}
			
	//Supprimer une map
	public function deleteMap($idMapRequest){
		$result = $this->_requete('INSERT INTO aom_map_deleted SELECT * FROM aom_map WHERE idMap=?;',array($idMapRequest));
		$result = $this->_requete('DELETE FROM aom_map WHERE idMap=?;',array($idMapRequest));
	}
	
	//Réinitialiser l'affichage des maps
	public function reinitMaps(){
		$result = $this->_requete('INSERT INTO aom_map SELECT * FROM aom_map_deleted');
		$result = $this->_requete('DELETE FROM aom_map_deleted');
	}
}
	