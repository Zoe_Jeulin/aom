<?php

require_once (PATH_MODELS."DAO.php");

class DieuDAO extends DAO {

    //Récupérer tous les dieux primaires d'une civilisation
    public function getDieuxPrimairesFromCivilisation($idCivilisationRequest){
        $result = $this->queryAll('SELECT * FROM aom_dieu WHERE  idCivilisation=? AND idAge=1;',array($idCivilisationRequest));
		return json_encode($result);
    }

    //Récupérer tous les dieux secondaires d'un dieu primaire grâce au contenu de son attribut "dieuxAccessibles" (stocké dans $idDieuxSecondaires)
    public function getDieuxSecondairesByIdString($idCivilisationRequest,$idDieuPrimaire, $idDieuxSecondaires){
        $result = $this->queryAll('SELECT * FROM aom_dieu WHERE  idCivilisation=? AND (idDieu = ? OR idDieu=? OR idDieu=? OR idDieu=? OR idDieu=? OR idDieu=? OR idDieu=?);',array_merge(array($idCivilisationRequest),array($idDieuPrimaire),$idDieuxSecondaires));
		return json_encode($result);
    }
}