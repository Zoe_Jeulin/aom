<?php

require_once (PATH_MODELS."DAO.php");

class AmeliorationDAO extends DAO {
    
    //Récupérer toutes les améliorations d'un bâtiment grâce à son id
    public function getBatimentsAmeliorations($idBatiment){
        $result = $this->queryAll('SELECT * FROM aom_amelioration WHERE idBatimentConcerne=?;',array($idBatiment));
		return json_encode($result);
    }
}