<?php

require_once (PATH_MODELS."DAO.php");

class UniteDAO extends DAO {

    //Récupérer toutes les unités d'une civilisation grâce à son id
    public function getUnitesFromCivilisation($idCivilisationRequest){
        $result = $this->queryAll('SELECT * FROM aom_unite WHERE idCivilisation=?;',array($idCivilisationRequest));
		return json_encode($result);
    }
}