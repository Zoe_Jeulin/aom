<?php

require_once (PATH_MODELS."DAO.php");

class CivilisationDAO extends DAO {
	
	//Récupérer toutes les civilisations
	public function getAllCivilisation(){
		$result = $this->queryAll('SELECT * FROM aom_civilisation');
		return json_encode($result);
	}
	
	//Récupérer seulement l'id d'une civilisation grâce à son nom
	public function getCivilisationId($nomCivilisationRequest){
		$nomCivilisationRequest=$nomCivilisationRequest.'.png';
		$result = $this->queryRow('SELECT idCivilisation FROM aom_civilisation WHERE imgCivilisation=?;',array($nomCivilisationRequest));
		return $result['idCivilisation'];
    }   
}

	