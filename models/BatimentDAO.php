<?php

require_once (PATH_MODELS."DAO.php");

class BatimentDAO extends DAO {

    //Récupérer tous les bâtiments d'une civilisation grâce à son id
    public function getBatimentsFromCivilisation($idCivilisationRequest){
        $result = $this->queryAll('SELECT * FROM aom_batiment WHERE idCivilisation=?;',array($idCivilisationRequest));
		return json_encode($result);
    }
}