<?php

function choixAlert($message, $var=null)
{
	$alert = array();
	switch($message)
	{
		//Messages d'erreurs
		case 'query' :
			$alert['messageAlert'] = ERREUR_QUERY;
			break;
		case 'url_non_valide' :
			$alert['messageAlert'] = TEXTE_PAGE_404;
			break;
		  
		//Messages d'informations
		
			
		//Message par défaut
		default :
		  $alert['messageAlert'] = MESSAGE_ERREUR;
	  }
  return $alert;
}
