Document.prototype.ready = callback => {
	if(callback && typeof callback === 'function') {
		document.addEventListener("DOMContentLoaded", () =>  {
			if(document.readyState === "interactive" || document.readyState === "complete") {
				return callback();
			}
		});
	}
};

document.ready( () => {
	//chargement des données des 3 onglets
	affichageDieux(); //Onglet actif
	affichageUnites();
	affichageBatiments();

	//permet l'affichage du tableau avec les onglets swipeable
	var el = document.querySelectorAll('.tabs');
	var instance = M.Tabs.init(el,{swipeable:true});
});

function affichageDieux(){
	//récupération des dieux primaires
	fetch("../../controllers/c_dieux.php")
		.then( response => { return response.json() })
		.then( data => {
			affichageOngletsDieux(data); //affichage des dieux primaires
			data.forEach(primaire => { 
				let param = {};
				param.idDieuPrimaire = primaire.idDieu;
				param.idDieuxSecondaires = primaire.dieuxAccessibles;
				
				//au clic sur un des onglets des dieux primaires, récupération de ses dieux secondaires
				let onglet = document.getElementById("onglet_"+primaire.nomDieu);
				onglet.addEventListener("click",function() {
					fetch("../../controllers/c_dieux.php",
						{method : "POST",
						body : JSON.stringify(param)})
							.then( response => { return response.json() })
							.then( dieuSec => {
								affichageContenuDieux(dieuSec); //affichage des dieux secondaires
							})
							.catch(error => { console.log(error) });
				});
			})
		})
		.catch(error => { console.log(error) });

}

//affichage des dieux primaires
function affichageOngletsDieux(data){
	let tousLesDieux = document.getElementById("allDieux");

	if(document.getElementById("primaires"))
	{
		document.getElementById("primaires").remove();
	}

	let ongletsDieuxPrimaires = document.createElement("div"); //div des onglets des 3 dieux primaires
	ongletsDieuxPrimaires.id = "primaires";
	ongletsDieuxPrimaires.className = "col s2";

	data.forEach( dieu => {
		//div d'onglet de chacun des 3 dieux
		let dieuPrimaireSelectionne = document.createElement("div");
		dieuPrimaireSelectionne.id = "onglet_"+dieu.nomDieu;
		dieuPrimaireSelectionne.className = "row eachPrimaires center-align";

		//image du dieu
		let imgDieuPrimaire = document.createElement("img"); 
		imgDieuPrimaire.src = "../../assets/images/dieux/"+dieu.imgDieu;
		imgDieuPrimaire.alt = "Image du dieu";
		imgDieuPrimaire.className = "img_onglet";

		//nom du dieu
		let pDieuPrimaire = document.createElement("p");
		pDieuPrimaire.innerHTML = dieu.nomDieu;
		
		//ajout de l'iage et du nom du dieu à l'onglet
		dieuPrimaireSelectionne.appendChild(imgDieuPrimaire);
		dieuPrimaireSelectionne.appendChild(pDieuPrimaire);

		//ajout de l'onglet à la div contenant les 3 onglets dieux
		ongletsDieuxPrimaires.appendChild(dieuPrimaireSelectionne);

		//divider css
		let divider = document.createElement("div");
		divider.className = "divider";
		ongletsDieuxPrimaires.appendChild(divider);
	});
	//ajout de la div contenant les 3 onglets dieux à l'onglet "dieux" du tableau swipeable
	tousLesDieux.appendChild(ongletsDieuxPrimaires);
}

function affichageContenuDieux(dieuxSecondaires){
	let divGlobale = document.getElementById("allDieux");
	if(document.getElementById("secondaires"))
	{
		document.getElementById("secondaires").remove();
	}
	let divDieuxSecondaires = document.createElement("div");
	divDieuxSecondaires.id = "secondaires";
	divDieuxSecondaires.className = "col s10";

	let divAge1 = document.createElement("div");
	divAge1.id = "age1";
	divAge1.className = "col s3 center-align";

	let divAge2 = document.createElement("div");
	divAge2.id = "age2";
	divAge2.className = "col s3 center-align";

	let divAge3 = document.createElement("div");
	divAge3.id = "age3";
	divAge3.className = "col s3 center-align";

	let divAge4 = document.createElement("div");
	divAge4.id = "age4";
	divAge4.className = "col s3 center-align";

	let nomAge1 = document.createElement("div")
	nomAge1.className = "divNomAge valign-wrapper";
	nomAge1.innerHTML = "Age Archaïque";

	let nomAge2 = document.createElement("div")
	nomAge2.className = "divNomAge valign-wrapper";
	nomAge2.innerHTML = "Age Classique";

	let nomAge3 = document.createElement("div")
	nomAge3.className = "divNomAge valign-wrapper";
	nomAge3.innerHTML = "Age Héroïque";

	let nomAge4 = document.createElement("div")
	nomAge4.className = "divNomAge valign-wrapper";
	nomAge4.innerHTML = "Age Mythique";

	divAge1.appendChild(nomAge1);
	divAge2.appendChild(nomAge2);
	divAge3.appendChild(nomAge3);
	divAge4.appendChild(nomAge4);
	

	dieuxSecondaires.forEach( dieu => {
		let divDieu = document.createElement("div"); //div englobant chaque dieu
		divDieu.className = "eachSecondaire valign-wrapper";
		let divLienModal = document.createElement("div"); //div du lien du modal
		let divModal = document.createElement("div"); //div du modal
		divModal.id = dieu.nomDieu;
		divModal.className = "modal";
		
		//lien pour ouvrir le modal
		let aLienModal = document.createElement("a");
		aLienModal.href = "#"+dieu.nomDieu;
		aLienModal.className = "modal-trigger";
		
		//image du lien
		let imgLienModal = document.createElement("img");
		imgLienModal.src = "../../assets/images/dieux/"+dieu.imgDieu;
		imgLienModal.alt = "Image du dieu";
		
		//nom du lien
		let pLienModal = document.createElement("p");
		pLienModal.innerHTML = dieu.nomDieu;
		
		//ajout de l'image et du nom au lien
		aLienModal.appendChild(imgLienModal);
		aLienModal.appendChild(pLienModal);
		
		//ajout du lien à la div lien
		divLienModal.appendChild(aLienModal);
		
		let divModalContent = document.createElement("div"); //div pour le contenu du modal
		divModalContent.className = "modal-content";
		let divModalFooter = document.createElement("div"); //div pour le footer du modal
		divModalFooter.className = "modal-footer";
		
		//nom du dieu dans le modal
		let pNomDieuModal = document.createElement("p");
		pNomDieuModal.innerHTML = dieu.nomDieu;
		
		//image du dieu dans le modal
		let imgDieuModal = document.createElement("img");
		imgDieuModal.src = "../../assets/images/dieux/"+dieu.imgDieu;
		imgDieuModal.alt = "Image du dieu";
		imgDieuModal.className = "img_petit";
		
		//description du dieu dans le modal
		let pDescriptionDieuModal = document.createElement("p");
		pDescriptionDieuModal.className = "description_dieu";
		pDescriptionDieuModal.innerHTML = dieu.descriptionDieu;
		
		//nom du pouvoir dans le modal
		let pNomPouvoirModal = document.createElement("p");
		pNomPouvoirModal.innerHTML = dieu.pouvoirDieu;
		
		//description du pouvoir dans le modal
		let pDescriptionPouvoirModal = document.createElement("p");
		pDescriptionPouvoirModal.className = "description_dieu";
		pDescriptionPouvoirModal.innerHTML = dieu.descriptionPouvoir;
		
		//ajout du contenu à la div contenu
		divModalContent.appendChild(imgDieuModal);
		divModalContent.appendChild(pNomDieuModal);
		divModalContent.appendChild(pDescriptionDieuModal);
		divModalContent.appendChild(pNomPouvoirModal);
		divModalContent.appendChild(pDescriptionPouvoirModal);
		
		//bouton (lien) pour fermer le modal
		let aFermerModal = document.createElement("a");
		aFermerModal.href = "#!";
		aFermerModal.className = "modal-close waves-effect waves-red btn-flat";
		aFermerModal.innerHTML = "Fermer";
		
		//ajout du bouton fermer à la div footer
		divModalFooter.appendChild(aFermerModal);
		
		//ajout de la div contenu et de la div footer à la div modal
		divModal.appendChild(divModalContent);
		divModal.appendChild(divModalFooter);
		
		//ajout de la div lien et de la div modal à la div du dieu
		divDieu.appendChild(divLienModal);
		divDieu.appendChild(divModal);
		
		//ajout de la div du dieu à la div englobant toutes les dieux
		switch(dieu.idAge)
		{
			case '1' : 
				divAge1.appendChild(divDieu);
				break;
			case '2' :
				divAge2.appendChild(divDieu);
				break;
			
			case '3' :
				divAge3.appendChild(divDieu);
				break;
			
			case '4' :
				divAge4.appendChild(divDieu);
				break;

			default :console.log("Error");
				break;
		}
		divDieuxSecondaires.appendChild(divAge1);
		divDieuxSecondaires.appendChild(divAge2);
		divDieuxSecondaires.appendChild(divAge3);
		divDieuxSecondaires.appendChild(divAge4);

		divGlobale.appendChild(divDieuxSecondaires);
	});
			
	//permet l'ouverture du modal
	var elems = document.querySelectorAll('.modal');
	var instances = M.Modal.init(elems);
}

function affichageUnites(){
	//récupération des unités de la civilisation
	fetch("../../controllers/c_unites")
		.then(response => { return response.json()})
		.then(data => {
			document.getElementById("divUnites").remove();
			let ongletUnites = document.getElementById("unites");
			let divUnites = document.createElement("div"); //div globale contenant toutes les unités
			divUnites.id = "divUnites";
			divUnites.className = "row";

			//div des unités de l'âge archaïque
			let divUniteAge1 = document.createElement("div");
			divUniteAge1.id = "divUniteAge1";
			divUniteAge1.className = "col s3";

			//div des unités de l'âge classique
			let divUniteAge2 = document.createElement("div");
			divUniteAge2.id = "divUniteAge2";
			divUniteAge2.className = "col s3";

			//div des unités de l'âge héroïque
			let divUniteAge3 = document.createElement("div");
			divUniteAge3.id = "divUniteAge3";
			divUniteAge3.className = "col s3";

			//div des unités de l'âge mythique
			let divUniteAge4 = document.createElement("div");
			divUniteAge4.id = "divUniteAge4";
			divUniteAge4.className = "col s3";

			//div nom âge archaïque
			let nomAge1Unit = document.createElement("div")
			nomAge1Unit.className = "divNomAge valign-wrapper col s3";
			nomAge1Unit.innerHTML = "Age Archaïque";

			//div nom âge classique
			let nomAge2Unit = document.createElement("div")
			nomAge2Unit.className = "divNomAge valign-wrapper col s3";
			nomAge2Unit.innerHTML = "Age Classique";

			//div nom âge héroïque
			let nomAge3Unit = document.createElement("div")
			nomAge3Unit.className = "divNomAge valign-wrapper col s3";
			nomAge3Unit.innerHTML = "Age Héroïque";

			//div nom âge mythique
			let nomAge4Unit = document.createElement("div")
			nomAge4Unit.className = "divNomAge valign-wrapper col s3";
			nomAge4Unit.innerHTML = "Age Mythique";

			//div contenant les 4 divs des noms des âges
			let ageHeader = document.createElement("div")
			ageHeader.className = "row  valign-wrapper titleAge";

			//ajout des 4 divs noms à la div englobante des noms
			ageHeader.appendChild(nomAge1Unit);
			ageHeader.appendChild(nomAge2Unit);
			ageHeader.appendChild(nomAge3Unit);
			ageHeader.appendChild(nomAge4Unit);

			//ajout des div des 4 âges à la div globale
			divUnites.appendChild(ageHeader);
			divUnites.appendChild(divUniteAge1);
			divUnites.appendChild(divUniteAge2);
			divUnites.appendChild(divUniteAge3);
			divUnites.appendChild(divUniteAge4);			

			data.forEach( unite => {				
				let divUnite = document.createElement("div"); //div englobant chaque unite
				divUnite.className = "col s4 eachUnit";
				let divLienModalUnite = document.createElement("div"); //div du lien du modal
				divLienModalUnite.className = "center-align";
				let divModalUnite = document.createElement("div"); //div du modal
				divModalUnite.id = unite.nomUnite;
				divModalUnite.className = "modal";
				
				//lien pour ouvrir le modal
				let aLienModalUnite = document.createElement("a");
				aLienModalUnite.href = "#"+unite.nomUnite;
				aLienModalUnite.className = "modal-trigger";
				
				//image du lien
				let imgLienModalUnite = document.createElement("img");
				imgLienModalUnite.className = "circle";
				imgLienModalUnite.src = "../../assets/images/unites/"+unite.imgUnite;
				imgLienModalUnite.alt = "Image de l'unite";
				
				//nom du lien
				let pLienModalUnite = document.createElement("p");
				pLienModalUnite.innerHTML = unite.nomUnite;
				
				//ajout de l'image et du nom au lien
				aLienModalUnite.appendChild(imgLienModalUnite);
				aLienModalUnite.appendChild(pLienModalUnite);
				
				//ajout du lien à la div lien
				divLienModalUnite.appendChild(aLienModalUnite);
				
				let divModalUniteContent = document.createElement("div"); //div pour le contenu du modal
				divModalUniteContent.className = "modal-content";
				let divModalUniteFooter = document.createElement("div"); //div pour le footer du modal
				divModalUniteFooter.className = "modal-footer";
				
				//nom de l'unité dans le modal
				let pNomUniteModal = document.createElement("p");
				pNomUniteModal.innerHTML = unite.nomUnite;
				
				//image de l'unité dans le modal
				let imgUniteModal = document.createElement("img");
				imgUniteModal.src = "../../assets/images/unites/"+unite.imgUnite;
				imgUniteModal.className = "img_unit_modal";
				imgUniteModal.alt = "Image de l'unité";
				//imgUniteModal.className = "img_petit";
				
				//description de l'unité dans le modal
				let pDescriptionUniteModal = document.createElement("p");
				pDescriptionUniteModal.className = "description_unit_modal";
				pDescriptionUniteModal.innerHTML = unite.descriptionUnite;
				
				//div contenant les coûts de l'unité
				let divCoutsUnites = document.createElement("div");
				divCoutsUnites.id="div_couts_unite";
				divCoutsUnites.className="row";
				
				//div du coût en nourriture
				let divNourriture = document.createElement("div");
				divNourriture.id="nourriture";
				divNourriture.className="col s3";
				
				//image nourriture
				let imgCoutNourriture = document.createElement("img");
				imgCoutNourriture.src = "../../assets/images/divers/nourriture.png";
				imgCoutNourriture.alt = "Image de nourriture";

				//coût nourriture
				let pCoutNourriture = document.createElement("p");
				pCoutNourriture.innerHTML = unite.coutNourriture;

				//ajout de l'image et du coût à la div coût nourriture
				divNourriture.appendChild(imgCoutNourriture);
				divNourriture.appendChild(pCoutNourriture);

				//div du coût en bois
				let divBois = document.createElement("div");
				divBois.id="bois";
				divBois.className="col s3";

				//image bois
				let imgCoutBois = document.createElement("img");
				imgCoutBois.src = "../../assets/images/divers/bois.png";
				imgCoutBois.alt = "Image de bois";

				//coût bois
				let pCoutBois = document.createElement("p");
				pCoutBois.innerHTML = unite.coutBois;

				//ajout de l'image et du coût à la div coût bois
				divBois.appendChild(imgCoutBois);
				divBois.appendChild(pCoutBois);

				//div du coût en or
				let divOr = document.createElement("div");
				divOr.id="or";
				divOr.className="col s3";

				//image or
				let imgCoutOr = document.createElement("img");
				imgCoutOr.src = "../../assets/images/divers/or.png";
				imgCoutOr.alt = "Image d'or";

				//coût or
				let pCoutOr = document.createElement("p");
				pCoutOr.innerHTML = unite.coutOr;

				//ajout de l'image et du coût à la div coût or
				divOr.appendChild(imgCoutOr);
				divOr.appendChild(pCoutOr);

				//div du coût en faveur
				let divFaveur = document.createElement("div");
				divFaveur.id="faveur";
				divFaveur.className="col s3";

				//image faveur
				let imgCoutFaveur = document.createElement("img");
				imgCoutFaveur.src = "../../assets/images/divers/faveur.png";
				imgCoutFaveur.alt = "Image de faveur";

				//coût faveur
				let pCoutFaveur = document.createElement("p");
				pCoutFaveur.innerHTML = unite.coutFaveur;

				//ajout de l'image et du coût à la div coût faveur
				divFaveur.appendChild(imgCoutFaveur);
				divFaveur.appendChild(pCoutFaveur);

				//ajout des 4 div des coûts à la div englobant les coûts
				divCoutsUnites.appendChild(divNourriture);
				divCoutsUnites.appendChild(divBois);
				divCoutsUnites.appendChild(divOr);
				divCoutsUnites.appendChild(divFaveur);

				//ajout du contenu du modal à la div contenu
				divModalUniteContent.appendChild(imgUniteModal);
				divModalUniteContent.appendChild(pNomUniteModal);
				divModalUniteContent.appendChild(pDescriptionUniteModal);
				divModalUniteContent.appendChild(divCoutsUnites);

				//bouton (lien) pour fermer le modal
				let aFermerModalUnite = document.createElement("a");
				aFermerModalUnite.href = "#!";
				aFermerModalUnite.className = "modal-close waves-effect waves-green btn-flat";
				aFermerModalUnite.innerHTML = "Fermer";
				
				//ajout du bouton fermer à la div footer
				divModalUniteFooter.appendChild(aFermerModalUnite);
				
				//ajout de la div contenu et de la div footer à la div modal
				divModalUnite.appendChild(divModalUniteContent);
				divModalUnite.appendChild(divModalUniteFooter);
				
				//ajout de la div lien et de la div modal à la div du unite
				divUnite.appendChild(divLienModalUnite);
				divUnite.appendChild(divModalUnite);

				//ajout de la div de chaque unité à la div de son âge respectif
				switch(unite.idAge)
				{
				case '1' : 
					divUniteAge1.appendChild(divUnite);
					break;
				case '2':
					divUniteAge2.appendChild(divUnite);
					break;
				case '3' : 
					divUniteAge3.appendChild(divUnite);
					break;
				case '4':
					divUniteAge4.appendChild(divUnite);
					break;
				}		
				ongletUnites.appendChild(divUnites);
			});
			//permet l'ouverture du modal
			var elems = document.querySelectorAll('.modal');
			var instances = M.Modal.init(elems);
		})
		.catch(error => { console.log(error) });

}

function affichageBatiments(){
	//récupération des bâtiments de la civilisation
	fetch("../../controllers/c_batiments")
		.then(response => { return response.json()})
		.then(data => {
			document.getElementById("divBatiments").remove();
			let ongletBatiments = document.getElementById("batiments");
			let divBatiments = document.createElement("div"); //div globale contenant tous les bâtiments
			divBatiments.id = "divBatiments";
			divBatiments.className = "row";
			
			//div des unités de l'âge archaïque
			let divBatimentAge1 = document.createElement("div");
			divBatimentAge1.id = "divBatimentAge1";
			divBatimentAge1.className = "col s3";

			//div des unités de l'âge classique
			let divBatimentAge2 = document.createElement("div");
			divBatimentAge2.id = "divBatimentAge2";
			divBatimentAge2.className = "col s3";

			//div des unités de l'âge héroïque
			let divBatimentAge3 = document.createElement("div");
			divBatimentAge3.id = "divBatimentAge3";
			divBatimentAge3.className = "col s3";

			//div des unités de l'âge mythique
			let divBatimentAge4 = document.createElement("div");
			divBatimentAge4.id = "divBatimentAge4";
			divBatimentAge4.className = "col s3";

			//div nom âge archaïque
			let nomAge1Batiment = document.createElement("div")
			nomAge1Batiment.className = "divNomAge valign-wrapper col s3";
			nomAge1Batiment.innerHTML = "Age Archaïque";

			//div nom âge classique
			let nomAge2Batiment = document.createElement("div")
			nomAge2Batiment.className = "divNomAge valign-wrapper col s3";
			nomAge2Batiment.innerHTML = "Age Classique";

			//div nom âge héroïque
			let nomAge3Batiment = document.createElement("div")
			nomAge3Batiment.className = "divNomAge valign-wrapper col s3";
			nomAge3Batiment.innerHTML = "Age Héroïque";

			//div nom âge mythique
			let nomAge4Batiment = document.createElement("div")
			nomAge4Batiment.className = "divNomAge valign-wrapper col s3";
			nomAge4Batiment.innerHTML = "Age Mythique";

			//div contenant les 4 divs des noms des âges
			let ageHeaderBatiment = document.createElement("div")
			ageHeaderBatiment.className = "row  valign-wrapper titleAge";

			//ajout des 4 divs noms à la div englobante des noms
			ageHeaderBatiment.appendChild(nomAge1Batiment);
			ageHeaderBatiment.appendChild(nomAge2Batiment);
			ageHeaderBatiment.appendChild(nomAge3Batiment);
			ageHeaderBatiment.appendChild(nomAge4Batiment);

			//ajout des div des 4 âges à la div globale
			divBatiments.appendChild(ageHeaderBatiment);
			divBatiments.appendChild(divBatimentAge1);
			divBatiments.appendChild(divBatimentAge2);
			divBatiments.appendChild(divBatimentAge3);
			divBatiments.appendChild(divBatimentAge4);

			data.forEach( batiment => {
				let divBatiment = document.createElement("div"); //div englobant chaque bâtiment
				divBatiment.className = "col s4 eachBatiment";
				let divLienModalBatiment = document.createElement("div"); //div du lien du modal
				divLienModalBatiment.className = "center-align";
				let divModalBatiment = document.createElement("div"); //div du modal
				divModalBatiment.id = batiment.nomBatiment;
				divModalBatiment.className = "modal";
				
				//lien pour ouvrir le modal
				let aLienModalBatiment = document.createElement("a");
				aLienModalBatiment.href = "#"+batiment.nomBatiment;
				aLienModalBatiment.className = "modal-trigger";
				
				//image du lien
				let imgLienModalBatiment = document.createElement("img");
				imgLienModalBatiment.className = "circle";
				imgLienModalBatiment.src = "../../assets/images/batiments/"+batiment.imgBatiment;
				imgLienModalBatiment.alt = "Image du batiment";
				
				//nom du lien
				let pLienModalBatiment = document.createElement("p");
				pLienModalBatiment.innerHTML = batiment.nomBatiment;
				
				//ajout de l'image et du nom au lien
				aLienModalBatiment.appendChild(imgLienModalBatiment);
				aLienModalBatiment.appendChild(pLienModalBatiment);
				
				//ajout du lien à la div lien
				divLienModalBatiment.appendChild(aLienModalBatiment);
				
				let divModalBatimentContent = document.createElement("div"); //div pour le contenu du modal
				divModalBatimentContent.className = "modal-content";
				let divModalBatimentFooter = document.createElement("div"); //div pour le footer du modal
				divModalBatimentFooter.className = "modal-footer";
				
				//nom du bâtiment dans le modal
				let pNomBatimentModal = document.createElement("p");
				pNomBatimentModal.innerHTML = batiment.nomBatiment;
				
				//image du bâtiment dans le modal
				let imgBatimentModal = document.createElement("img");
				imgBatimentModal.src = "../../assets/images/batiments/"+batiment.imgBatiment;
				imgBatimentModal.alt = "Image du batiment";
				//imgBatimentModal.className = "img_petit";
				
				//description du bâtiment dans le modal
				let pDescriptionBatimentModal = document.createElement("p");
				pDescriptionBatimentModal.className = "description_batiment";
				pDescriptionBatimentModal.innerHTML = batiment.descriptionBatiment;
				
				//div contenant les coûts du bâtiment
				let divCoutsBatiment = document.createElement("div");
				divCoutsBatiment.id="div_couts_batiment";
				divCoutsBatiment.className="row";

				//div du coût en nourriture
				let divNourriture = document.createElement("div");
				divNourriture.id="nourriture";
				divNourriture.className="col s3";

				//image nourriture
				let imgCoutNourriture = document.createElement("img");
				imgCoutNourriture.src = "../../assets/images/divers/nourriture.png";
				imgCoutNourriture.alt = "Image de nourriture";

				//coût nourriture
				let pCoutNourriture = document.createElement("p");
				pCoutNourriture.innerHTML = batiment.coutNourriture;

				//ajout de l'image et du coût à la div coût nourriture
				divNourriture.appendChild(imgCoutNourriture);
				divNourriture.appendChild(pCoutNourriture);

				//div du coût en bois
				let divBois = document.createElement("div");
				divBois.id="bois";
				divBois.className="col s3";

				//image bois
				let imgCoutBois = document.createElement("img");
				imgCoutBois.src = "../../assets/images/divers/bois.png";
				imgCoutBois.alt = "Image de bois";

				//coût bois
				let pCoutBois = document.createElement("p");
				pCoutBois.innerHTML = batiment.coutBois;

				//ajout de l'image et du coût à la div coût bois
				divBois.appendChild(imgCoutBois);
				divBois.appendChild(pCoutBois);

				//div du coût en or
				let divOr = document.createElement("div");
				divOr.id="or";
				divOr.className="col s3";

				//image or
				let imgCoutOr = document.createElement("img");
				imgCoutOr.src = "../../assets/images/divers/or.png";
				imgCoutOr.alt = "Image d'or";

				//coût or
				let pCoutOr = document.createElement("p");
				pCoutOr.innerHTML = batiment.coutOr;

				//ajout de l'image et du coût à la div coût or
				divOr.appendChild(imgCoutOr);
				divOr.appendChild(pCoutOr);

				//div du coût en faveur
				let divFaveur = document.createElement("div");
				divFaveur.id="faveur";
				divFaveur.className="col s3";

				//image faveur
				let imgCoutFaveur = document.createElement("img");
				imgCoutFaveur.src = "../../assets/images/divers/faveur.png";
				imgCoutFaveur.alt = "Image de faveur";

				//coût faveur
				let pCoutFaveur = document.createElement("p");
				pCoutFaveur.innerHTML = batiment.coutFaveur;

				//ajout de l'image et du coût à la div coût faveur
				divFaveur.appendChild(imgCoutFaveur);
				divFaveur.appendChild(pCoutFaveur);

				//ajout des 4 div des coûts à la div englobant les coûts
				divCoutsBatiment.appendChild(divNourriture);
				divCoutsBatiment.appendChild(divBois);
				divCoutsBatiment.appendChild(divOr);
				divCoutsBatiment.appendChild(divFaveur);

				//ajout du contenu du modal à la div contenu
				divModalBatimentContent.appendChild(imgBatimentModal);
				divModalBatimentContent.appendChild(pNomBatimentModal);
				divModalBatimentContent.appendChild(pDescriptionBatimentModal);
				divModalBatimentContent.appendChild(divCoutsBatiment);

				//récupération d'une div contenant les améliorations disponibles pour ce bâtiment
				let divAmeliorations = affichageAmeliorations(batiment.idBatiment);

				//ajout de la div des améliorations au contenu du modal du bâtiment
				divModalBatimentContent.appendChild(divAmeliorations);

				//bouton (lien) pour fermer le modal
				let aFermerModalBatiment = document.createElement("a");
				aFermerModalBatiment.href = "#!";
				aFermerModalBatiment.className = "modal-close waves-effect waves-green btn-flat";
				aFermerModalBatiment.innerHTML = "Fermer";
				
				//ajout du bouton fermer à la div footer
				divModalBatimentFooter.appendChild(aFermerModalBatiment);
				
				//ajout de la div contenu et de la div footer à la div modal
				divModalBatiment.appendChild(divModalBatimentContent);
				divModalBatiment.appendChild(divModalBatimentFooter);
				
				//ajout de la div lien et de la div modal à la div du batiment
				divBatiment.appendChild(divLienModalBatiment);
				divBatiment.appendChild(divModalBatiment);
				
				//ajout de la div du batiment à la div de l'âge correspondant
				switch(batiment.idAge)
				{
				case '1' : 
					divBatimentAge1.appendChild(divBatiment);
					break;
				case '2':
					divBatimentAge2.appendChild(divBatiment);
					break;
				case '3' : 
					divBatimentAge3.appendChild(divBatiment);
					break;
				case '4':
					divBatimentAge4.appendChild(divBatiment);
					break;
				}		

				ongletBatiments.appendChild(divBatiments);
			});
			//permet l'ouverture du modal
			var elems = document.querySelectorAll('.modal');
			var instances = M.Modal.init(elems);
		})
		.catch(error => { console.log(error) });

}

function affichageAmeliorations(idBatiment){
	let divAmeliorations = document.createElement("div");
	let pAmeliorations = document.createElement("p");
	//récupération des améliorations du bâtiment
	fetch("../../controllers/c_ameliorations.php",
		{method : "POST",
		body : JSON.stringify(idBatiment)})
			.then(response => { return response.json()})
			.then(data => {
				if(data.length){
					pAmeliorations.innerHTML = "Améliorations : ";
					data.forEach(amelioration => {
						//div globale contenant toutes les améliorations de ce bâtiment
						let divInfosAmelioration = document.createElement("div"); 
								
						//nom de l'amélioration dans le tooltip
						let pNomAmelioration = document.createElement("p");
						pNomAmelioration.innerHTML = amelioration.nomAmelioration;

						//description de l'amélioration dans le tooltip
						let pDescriptionAmelioration = document.createElement("p");
						pDescriptionAmelioration.innerHTML = amelioration.descriptionAmelioration;

						//div contenant les coûts de l'amélioration
						let divCoutsAmelioration = document.createElement("div");
						divCoutsAmelioration.id="div_couts_amelioration";
						divCoutsAmelioration.className="row";

						//div du coût en nourriture
						let divNourriture = document.createElement("div");
						divNourriture.id="nourriture";
						divNourriture.className="col s3";
						
						//image nourriture
						let imgCoutNourriture = document.createElement("img");
						imgCoutNourriture.src = "../../assets/images/divers/nourriture.png";
						imgCoutNourriture.alt = "Image de nourriture";

						//coût nourriture
						let pCoutNourriture = document.createElement("p");
						pCoutNourriture.innerHTML = amelioration.coutNourriture;

						//ajout de l'image et du coût à la div coût nourriture
						divNourriture.appendChild(imgCoutNourriture);
						divNourriture.appendChild(pCoutNourriture);

						//div du coût en bois
						let divBois = document.createElement("div");
						divBois.id="bois";
						divBois.className="col s3";

						//image bois
						let imgCoutBois = document.createElement("img");
						imgCoutBois.src = "../../assets/images/divers/bois.png";
						imgCoutBois.alt = "Image de bois";

						//coût bois
						let pCoutBois = document.createElement("p");
						pCoutBois.innerHTML = amelioration.coutBois;

						//ajout de l'image et du coût à la div coût bois
						divBois.appendChild(imgCoutBois);
						divBois.appendChild(pCoutBois);

						//div du coût en or
						let divOr = document.createElement("div");
						divOr.id="or";
						divOr.className="col s3";

						//image or
						let imgCoutOr = document.createElement("img");
						imgCoutOr.src = "../../assets/images/divers/or.png";
						imgCoutOr.alt = "Image d'or";

						//coût or
						let pCoutOr = document.createElement("p");
						pCoutOr.innerHTML = amelioration.coutOr;

						//ajout de l'image et du coût à la div coût or
						divOr.appendChild(imgCoutOr);
						divOr.appendChild(pCoutOr);

						//div du coût en faveur
						let divFaveur = document.createElement("div");
						divFaveur.id="faveur";
						divFaveur.className="col s3";

						//image faveur
						let imgCoutFaveur = document.createElement("img");
						imgCoutFaveur.src = "../../assets/images/divers/faveur.png";
						imgCoutFaveur.alt = "Image de faveur";

						//coût faveur
						let pCoutFaveur = document.createElement("p");
						pCoutFaveur.innerHTML = amelioration.coutFaveur;

						//ajout de l'image et du coût à la div coût faveur
						divFaveur.appendChild(imgCoutFaveur);
						divFaveur.appendChild(pCoutFaveur);

						//ajout des 4 div des coûts à la div englobant les coûts
						divCoutsAmelioration.appendChild(divNourriture);
						divCoutsAmelioration.appendChild(divBois);
						divCoutsAmelioration.appendChild(divOr);
						divCoutsAmelioration.appendChild(divFaveur);

						//ajout nom/description/coûts à la div englobant l'amélioration
						divInfosAmelioration.appendChild(pNomAmelioration);
						divInfosAmelioration.appendChild(pDescriptionAmelioration);
						divInfosAmelioration.appendChild(divCoutsAmelioration);

						//image de l'amélioration
						let imgAmelioration = document.createElement("img");
						imgAmelioration.src = "../../assets/images/ameliorations/"+amelioration.imgAmelioration;
						imgAmelioration.alt = "Image de l'amélioration";
						imgAmelioration.className = "tooltipped";
						imgAmelioration.setAttribute("data-position","bottom");
						imgAmelioration.setAttribute("data-html","true");
						imgAmelioration.setAttribute("data-tooltip",divInfosAmelioration.innerHTML);

						divAmeliorations.appendChild(imgAmelioration);
					});
					var elems = document.querySelectorAll('.tooltipped');
					var instances = M.Tooltip.init(elems,{html : true});
				}
			})
			.catch(error => { console.log(error) });
	
	divAmeliorations.appendChild(pAmeliorations);
	//renvoi de la div des améliorations à la div du bâtiment
	return divAmeliorations;
}