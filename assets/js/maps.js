Document.prototype.ready = callback => {
	if(callback && typeof callback === 'function') {
		document.addEventListener("DOMContentLoaded", () =>  {
			if(document.readyState === "interactive" || document.readyState === "complete") {
				return callback();
			}
		});
	}
};

document.ready( () => {
	//bouton de tri par ordre alphabétique
	let btnOrdreAlpha = document.getElementById("btn_ordre_alpha");
	btnOrdreAlpha.addEventListener("click", function(){
		fetch("../controllers/c_maps.php",
			{method : "POST",
			body : JSON.stringify(1)}) //tri par ordre alphabétique
			.then( response => { return response.json() })
			.then( data => {
				afficherMaps(data);
				
				//permet l'ouverture des modals
				var elems = document.querySelectorAll('.modal');
				var instances = M.Modal.init(elems);
			})
		.catch(error => { console.log(error) });
	});
	
	//bouton de tri par ordre des favoris
	let btnOrdreFav = document.getElementById("btn_ordre_fav");
	btnOrdreFav.addEventListener("click", function(){
		fetch("../controllers/c_maps.php",
			{method : "POST",
			body : JSON.stringify(2)}) //tri par ordre des favoris
			.then( response => { return response.json() })
			.then( data => {
				afficherMaps(data);
				
				//permet l'ouverture des modals
				var elems = document.querySelectorAll('.modal');
				var instances = M.Modal.init(elems);
			})
		.catch(error => { console.log(error) });
	});
	
	//bouton de réinitialisation de l'affichage des maps (réafficher toutes les maps supprimées)
	let btnReinit = document.getElementById("btn_reinit");
	btnReinit.addEventListener("click", function(){
		fetch("../controllers/c_maps.php",
			{method : "POST",
			body : JSON.stringify(3)}) //réinitialisation des maps
			.then( response => { return response.json() })
			.then( data => {
				afficherMaps(data);
				
				//permet l'ouverture des modals
				var elems = document.querySelectorAll('.modal');
				var instances = M.Modal.init(elems);
			})
		.catch(error => { console.log(error) });
	});
	
	//récupération et affichage des maps au chargement de la page
	fetch("../controllers/c_maps.php",
		{method : "POST",
		body : JSON.stringify(1)}) //par défaut, tri par ordre alphabétique
		.then( response => { return response.json() })
		.then( data => {
			afficherMaps(data);
			
			//permet l'ouverture du modal
			var elems = document.querySelectorAll('.modal');
			var instances = M.Modal.init(elems);
		})
		.catch(error => { console.log(error) });
});

//affichage des maps au chargement de la page ou au clic sur l'un des boutons
function afficherMaps(data){
	document.getElementById("liste-maps").innerHTML="";
	let maps = document.getElementById('liste-maps'); //div englobant toutes les maps
	data.forEach( map => {
		let div1 = document.createElement("div"); //div englobant chaque map
		div1.className = "col s3 center-align eachMap";
		let div2a = document.createElement("div"); //div du lien du modal
		let div2b = document.createElement("div"); //div du modal
		div2b.id = map.nomMap;
		div2b.className = "modal";
		
			let divBandeauFav = document.createElement("div");
			divBandeauFav.className = "divBandeauFav";
		
		
		//lien pour ouvrir le modal
		let aLienModal = document.createElement("a");
		aLienModal.href = "#"+map.nomMap;
		aLienModal.className = "modal-trigger";
		
		//image du lien
		let imgLienModal = document.createElement("img");
		imgLienModal.src = "../assets/images/maps/"+map.imgMap;
		imgLienModal.alt = "Image de la map";
		imgLienModal.className = "img_map";
		
		//nom du lien
		let pLienModal = document.createElement("p");
		if(map.enFavori==1){
			pLienModal.innerHTML = "<i class=\"material-icons\">star</i>"+map.nomMap;
		}else{
			pLienModal.innerHTML = map.nomMap;
		}
		
		pLienModal.className = "nom_map";
		
		//ajout de l'image et du nom au lien
		aLienModal.appendChild(imgLienModal);
		aLienModal.appendChild(pLienModal);
		
		//ajout du lien à la div lien
		div2a.appendChild(aLienModal);
		
		let div2bContent = document.createElement("div"); //div pour le contenu du modal
		div2bContent.className = "modal-content";
		let div2bFooter = document.createElement("div"); //div pour le footer du modal
		div2bFooter.className = "modal-footer";
		
		//nom de la map dans le modal
		let pNomMapModal = document.createElement("p");
		pNomMapModal.innerHTML = map.nomMap;
		pNomMapModal.className = "titre_map_modal";
		
		//image de la map dans le modal
		let imgMapModal = document.createElement("img");
		imgMapModal.src = "../assets/images/maps/"+map.imgMap;
		imgMapModal.alt = "Image de la map";
		imgMapModal.className = "img_map_modal";
		
		//description de la map dans le modal
		let pDescriptionMapModal = document.createElement("p");
		pDescriptionMapModal.innerHTML = map.descriptionMap;
		pDescriptionMapModal.className = "description_map_modal";
		
		//ajout du contenu à la div contenu
		div2bContent.appendChild(pNomMapModal);
		if (map.enFavori==1){
			console.log("jtm2");
			div2bContent.appendChild(divBandeauFav);
		}
		div2bContent.appendChild(imgMapModal);
		div2bContent.appendChild(pDescriptionMapModal);
		
		//bouton (lien) pour ajouter en favori la map
		let aAjouterFavoris = document.createElement("a");
		aAjouterFavoris.className = "modal-close waves-effect waves-green btn-flat";
		if(map.enFavori==0){
			aAjouterFavoris.innerHTML = "Ajouter aux favoris";
		}else{
			aAjouterFavoris.innerHTML = "Supprimer des favoris";
		}
		aAjouterFavoris.addEventListener("click",function() {
			fetch("../controllers/c_maps.php",
				{method : "PUT",
				body : JSON.stringify(map.idMap)})
				.then( response => { return response.json() })
				.then( data => { 
					afficherMaps(data);

					//permet l'ouverture des modals
					var elems = document.querySelectorAll('.modal');
					var instances = M.Modal.init(elems);
				})
				.catch(error => { console.log(error) });
		});
		
		//bouton (lien) pour supprimer la map
		let aSupprimerMap = document.createElement("a");
		aSupprimerMap.className = "modal-close waves-effect waves-green btn-flat";
		aSupprimerMap.innerHTML = "Supprimer";
		aSupprimerMap.addEventListener("click",function() {
			fetch("../controllers/c_maps.php",
				{method : "DELETE",
				body : JSON.stringify(map.idMap)})
				.then( response => { return response.json() })
				.then( data => { 
					afficherMaps(data);

					//permet l'ouverture des modals
					var elems = document.querySelectorAll('.modal');
					var instances = M.Modal.init(elems);
				})
				.catch(error => { console.log(error) });
		});
						
		//bouton (lien) pour fermer le modal
		let aFermerModal = document.createElement("a");
		aFermerModal.href = "#!";
		aFermerModal.className = "modal-close waves-effect waves-green btn-flat";
		aFermerModal.innerHTML = "Fermer";
		
		//ajout des 3 boutons à la div footer
		div2bFooter.appendChild(aAjouterFavoris);
		div2bFooter.appendChild(aSupprimerMap);
		div2bFooter.appendChild(aFermerModal);
		
		//ajout de la div contenu et de la div footer à la div modal
		div2b.appendChild(div2bContent);
		div2b.appendChild(div2bFooter);
		
		//ajout de la div lien et de la div modal à la div de la map
		div1.appendChild(div2a);
		div1.appendChild(div2b);
		
		//ajout de la div de la map à la div englobant toutes les maps
		maps.appendChild(div1);
	});	
}