Document.prototype.ready = callback => {
	if(callback && typeof callback === 'function') {
		document.addEventListener("DOMContentLoaded", () =>  {
			if(document.readyState === "interactive" || document.readyState === "complete") {
				return callback();
			}
		});
	}
};

document.ready( () => {
		fetch("../controllers/c_civilisations.php") //récupération de toutes les civilisations
		.then( response => { return response.json() })
		.then( data => {
			let civilisations = document.getElementById("div_liste_civilisations"); //div englobant tous les liens des civilisations
			data.forEach( civilisation => {
				let div1 = document.createElement("div"); //div englobant chaque lien de civilisation
				div1.className="col s2 5civi";
				div1.id="row_"+civilisation.imgCivilisation.substr(0,civilisation.imgCivilisation.length-4);
				
				//p dans lequel on mettra le a (utile pour le css)
				let pLien = document.createElement("p");
				pLien.className="liens_page_civilisations center-align";
				
				//lien de chaque civilisation
				let aLienCivilisation = document.createElement("a");
				aLienCivilisation.href = window.location+"/"+civilisation.imgCivilisation.substr(0,civilisation.imgCivilisation.length-4);
				aLienCivilisation.innerHTML = civilisation.nomCivilisation.toUpperCase();
				aLienCivilisation.className = "liens_page_civilisations";
				
				//ajout du lien au p
				pLien.appendChild(aLienCivilisation);
				
				//ajout du p à la div
				div1.appendChild(pLien);
				
				//le lien vers chaque civilisation marche aussi en cliquant n'importe où sur la div
				div1.addEventListener('click', function(){
					window.location += "/"+civilisation.imgCivilisation.substr(0,civilisation.imgCivilisation.length-4);
				});
				
				//ajout de la div du lien à la div englobant tous les liens des civilisations
				civilisations.appendChild(div1);
			});			
		})
		.catch(error => { console.log(error) });
});

